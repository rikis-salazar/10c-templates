#include <iostream>

// Aliases
using std::cout;
using std::ostream;
using std::string;


// Forward declarations
class Fraction;
bool operator<( const Fraction& lhs, const Fraction& rhs );


class ReverseOrder{
   public:
      bool operator()( const Fraction& a, const Fraction& b) const{
         return b < a ;
	 // Note: operator> is not defined [nor needed]
      }
};


class RandomOrder{
   public:
      bool operator()( const Fraction& a, const Fraction& b) const{
         int r = rand() % 2 ;  // r is 0 or 1 with equal prob.
         return r == 0 ? true : false ;
      }
};


// Incomplete Fraction class
class Fraction{
   private:
      int num;
      int denom;
   public:
      Fraction(int n=0, int d=1)
         : num(n) , denom(d) {}
   
   friend bool operator<( const Fraction& lhs, const Fraction& rhs );
   friend ostream& operator<<( ostream& out, const Fraction& rhs );
}; 

bool operator<( const Fraction& lhs, const Fraction& rhs ){
   return lhs.num * rhs.denom < rhs.num * lhs.denom;
}

ostream& operator<<( ostream& out, const Fraction& rhs ){
   out << rhs.num << "/" << rhs.denom;
   return out;
}


// Our finalized template
template<typename T, typename S, typename R = T >
R Pic10C_min( const T& a, const S& b , const R& c = R() ){ 
   if ( a < b )
      return static_cast<R>(a);
   return static_cast<R>(b);
}



// Our custom min function
template <typename T, typename CMP>
T custom_min( const T& lhs , const T& rhs, CMP cmp ) {
   if ( cmp(lhs,rhs) )
      return lhs;
   return rhs;
}


int main(){

   int n1 = 1;
   int n2 = 3;
   double d1 = 1.2;
   double d2 = 3.4;
   Fraction f1(1,3);
   Fraction f2(2,4);
   string s1 = "Hola";
   string s2 = "Mundo";

   cout << "What about custom comparison?\n";
   cout << "Between " << d1 << " and " << d2 << " the custom min (aka max) is : ";
   cout << custom_min(d1,d2,ReverseOrder()) << "\n";
   
   cout << "\n";
   cout << "Random order ... just for fun!\n";
   cout << "Between " << d1 << " and " << d2 << " the random min is : ";
   cout << custom_min(d1,d2,RandomOrder()) << "\n";
   cout << "Between " << d1 << " and " << d2 << " the random min is : ";
   cout << custom_min(d1,d2,RandomOrder()) << "\n";
   cout << "Between " << d1 << " and " << d2 << " the random min is : ";
   cout << custom_min(d1,d2,RandomOrder()) << "\n";

   return 0;
}
