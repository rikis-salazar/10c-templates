# Templates [revisited]

This is an evolving handout. For now ~~I will only list the names of the files
provided in this repository, as well as a short description of the point they
are trying to illustrate.~~ only the `.cpp` examples can be found within this
repository. If time permits, I will add a short description here, as well as
a link to [`cpp.sh`][cpp_sh] where you can test these files.

[cpp_sh]: http://cpp.sh/

--- 

[Obviously incomplete] list of included files:

- [`readme.md`][read-me];
    this _readme_ file (`MarkDown` format).
- [`readme.md.pdf`][read-me-pdf];
    this _readme_ file (`pdf` format).

[read-me]: readme.md
[read-me-pdf]: readme.md
