#include <iostream>

// Aliases
using std::cout;
using std::ostream;
using std::string;


// Forward declarations
class Fraction;
bool operator<( const Fraction& lhs, const Fraction& rhs );


class ReverseOrder{
   public:
      bool operator()( const Fraction& a, const Fraction& b) const{
         return b < a ;
	 // Note: operator> is not defined [nor needed]
      }
};


// Incomplete Fraction class
class Fraction{
   private:
      int num;
      int denom;
   public:
      Fraction(int n=0, int d=1)
         : num(n) , denom(d) {}
   
   friend bool operator<( const Fraction& lhs, const Fraction& rhs );
   friend ostream& operator<<( ostream& out, const Fraction& rhs );
}; 

bool operator<( const Fraction& lhs, const Fraction& rhs ){
   return lhs.num * rhs.denom < rhs.num * lhs.denom;
}

ostream& operator<<( ostream& out, const Fraction& rhs ){
   out << rhs.num << "/" << rhs.denom;
   return out;
}


// Our finalized template
template<typename T, typename S, typename R = T >
R Pic10C_min( const T& a, const S& b , const R& c = R() ){ 
   if ( a < b )
      return static_cast<R>(a);
   return static_cast<R>(b);
}



// A custom minimum function
template <typename T, typename CMP>
T custom_min( const T& lhs , const T& rhs, CMP cmp ) {
   if ( cmp(lhs,rhs) )
      return lhs;
   return rhs;
}


int main(){

   int n1 = 1;
   int n2 = 3;
   cout << "Between " << n1 << " and " << n2 << " the minimum is : ";
   cout << Pic10C_min(n1,n2) << "\n";

   double d1 = 1.2;
   double d2 = 3.4;
   cout << "Between " << d1 << " and " << d2 << " the minimum is : ";
   cout << Pic10C_min(d1,d2) << "\n";

   Fraction f1(1,3);
   Fraction f2(2,4);
   cout << "Between " << f1 << " and " << f2 << " the minimum is : ";
   cout << Pic10C_min(f1,f2) << "\n";

   string s1 = "Hola";
   string s2 = "Mundo";
   cout << "Between " << s1 << " and " << s2 << " the minimum is : ";
   cout << Pic10C_min(s1,s2) << "\n";


   cout << "What about different types?\n";
   cout << "Between " << f1 << " and " << n2 << " the minimum is : ";
   cout << Pic10C_min(f1,n2) << "\n";

   cout << "What about int and double types?\n";
   cout << "Between " << n1 << " and " << d2 << " the minimum is : ";
   cout << Pic10C_min(n1,d2) << "\n";


   cout << "What about custom comparison?\n";
   cout << "Between " << d1 << " and " << d2 << " the custom min (aka max) is : ";
   cout <<custom_min(d1,d2,ReverseOrder()) << "\n";
   
   return 0;
}
