#include <iostream>

// Aliases
using std::cout;
using std::ostream;
using std::string;


// Incomplete Fraction class
class Fraction{
   private:
      int num;
      int denom;
   public:
      Fraction(int n=0, int d=1)
      // explicit Fraction(int n=0, int d=1)    // <-- 3:) 
         : num(n) , denom(d) {}
   
   friend bool operator<( const Fraction& lhs, const Fraction& rhs );
   friend ostream& operator<<( ostream& out, const Fraction& rhs );
}; 

bool operator<( const Fraction& lhs, const Fraction& rhs ){
   return lhs.num * rhs.denom < rhs.num * lhs.denom;
}

ostream& operator<<( ostream& out, const Fraction& rhs ){
   out << rhs.num << "/" << rhs.denom;
   return out;
}


/**
   SCRATCH THIS!!! It is ambiguous 

template<typename T, typename S, typename R>
R Pic10C_min( const T& a, const S& b ){
   if ( a < b )
      return static_cast<R>(a);
   return static_cast<R>(b);
}

*/


// Ok but...  min between different 
// Idea: Add a third template param 
template<typename T, typename S, typename R>
R Pic10C_min( const T& a, const S& b , const R& c){
   if ( a < b )
      return static_cast<R>(a);
   return static_cast<R>(b);
}


int main(){

   int n1 = 1;
   int n2 = 3;
   cout << "Between " << n1 << " and " << n2 << " the minimum is : ";
   cout << Pic10C_min(n1,n2) << "\n";

   double d1 = 1.2;
   double d2 = 3.4;
   cout << "Between " << d1 << " and " << d2 << " the minimum is : ";
   cout << Pic10C_min(d1,d2) << "\n";

   Fraction f1(1,3);
   Fraction f2(2,4);
   // Only works if operator<< is defined for Fraction objects
   cout << "Between " << f1 << " and " << f2 << " the minimum is : ";
   cout << Pic10C_min(f1,f2) << "\n";


   cout << "What about different types?\n";
   cout << "Between " << f1 << " and " << n2 << " the minimum is : ";
   cout << Pic10C_min(f1,n2) << "\n";
   // Note:              ^^           implicit conversion to Fraction
    

   cout << "Let's try again...\n";
   cout << "Between " << n2 << " and " << f1 << " the minimum is : ";
   cout << Pic10C_min(Fraction(n2),f1) << "\n";
   // Note:           ^^^^^^^^^^^^    explicit conversion to Fraction


   cout << "What about int and double types?\n";
   cout << "Between " << n1 << " and " << d2 << " the minimum is : ";
   cout << Pic10C_min(n1,d2) << "\n";
   //                 ^^^^^     is the return int, or is it double?

   return 0;
}
