#include <iostream>

// Aliases
using std::cout;
using std::ostream;
using std::string;


// Incomplete Fraction class
class Fraction{
   private:
      int num;
      int denom;
   public:
      Fraction(int n=0, int d=1)
         : num(n) , denom(d) {}
   
   friend bool operator<( const Fraction& lhs, const Fraction& rhs );
   friend ostream& operator<<( ostream& out, const Fraction& rhs );
}; 

bool operator<( const Fraction& lhs, const Fraction& rhs ){
   return lhs.num * rhs.denom < rhs.num * lhs.denom;
}

ostream& operator<<( ostream& out, const Fraction& rhs ){
   out << rhs.num << "/" << rhs.denom;
   return out;
}


// Our comparison functions
int Pic10C_min( int a, int b ){
   if ( a < b ) 
      return a;
   return b;
}

double Pic10C_min( double a, double b ){
   if ( a < b ) 
      return a;
   return b;
}

// Only works if operator< is defined for Fraction objects
const Fraction& Pic10C_min( const Fraction& a, const Fraction& b ){
   if ( a < b ) 
      return a;
   return b;
}


int main(){

   int n1 = 1;
   int n2 = 3;
   cout << "Between " << n1 << " and " << n2 << " the minimum is : ";
   cout << Pic10C_min(n1,n2) << "\n";

   double d1 = 1.2;
   double d2 = 3.4;
   cout << "Between " << d1 << " and " << d2 << " the minimum is : ";
   cout << Pic10C_min(d1,d2) << "\n";

   Fraction f1(1,3);
   Fraction f2(2,4);
   // Only works if operator<< is defined for Fraction objects
   cout << "Between " << f1 << " and " << f2 << " the minimum is : ";
   cout << Pic10C_min(f1,f2) << "\n";


   return 0;
}
