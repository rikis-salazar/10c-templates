#include <iostream>

// Aliases
using std::cout;
using std::ostream;
using std::string;


// Incomplete Fraction class
class Fraction{
   private:
      int num;
      int denom;
   public:
      Fraction(int n=0, int d=1)
      // explicit Fraction(int n=0, int d=1)    // <-- 3:) 
         : num(n) , denom(d) {}
   
   friend bool operator<( const Fraction& lhs, const Fraction& rhs );
   friend ostream& operator<<( ostream& out, const Fraction& rhs );
}; 

bool operator<( const Fraction& lhs, const Fraction& rhs ){
   return lhs.num * rhs.denom < rhs.num * lhs.denom;
}

ostream& operator<<( ostream& out, const Fraction& rhs ){
   out << rhs.num << "/" << rhs.denom;
   return out;
}

// Ok but...  min between different 
// Idea: Add a third template param 
template<typename T, typename S, typename R>
// R Pic10C_min( const T& a, const S& b , const R& c ){    // Needs 3 param
R Pic10C_min( const T& a, const S& b , const R& c = R() ){ // 2 are OK
   if ( a < b )
      return static_cast<R>(a);
   return static_cast<R>(b);
}


int main(){

   /**
     // " ... add a third template param". They said.
     // " ... it would solve ambiguity". They said.
  
     // Yeah, right!!
   int n1 = 1;
   int n2 = 3;
   cout << "Between " << n1 << " and " << n2 << " the minimum is : ";
   cout << Pic10C_min(n1,n2) << "\n";

   double d1 = 1.2;
   double d2 = 3.4;
   cout << "Between " << d1 << " and " << d2 << " the minimum is : ";
   cout << Pic10C_min(d1,d2) << "\n";

   Fraction f1(1,3);
   Fraction f2(2,4);
   // Only works if operator<< is defined for Fraction objects
   cout << "Between " << f1 << " and " << f2 << " the minimum is : ";
   cout << Pic10C_min(f1,f2) << "\n";

   string s1 = "Hola";
   string s2 = "Mundo";
   cout << "Between " << s1 << " and " << s2 << " the minimum is : ";
   cout << Pic10C_min(s1,s2) << "\n";
*/
   int n1 = 1;
   int n2 = 3;
   double d1 = 1.2;
   double d2 = 3.4;
   Fraction f1(1,3);
   Fraction f2(2,4);


   cout << "What about different types?\n";
   cout << "Between " << f1 << " and " << n2 << " the minimum is : ";
   cout << Pic10C_min(f1,n2,f2) << "\n";
   // Note:                 ^^ Not used. Only specifies return type.


   cout << "What about int and double types?\n";
   cout << "Between " << n1 << " and " << d2 << " the minimum is : ";
   cout << Pic10C_min(n1,d2,0.01) << "\n";
   // Note:                 ^^^^ We want a double back.


   cout << "\nLet's try again...\n\n";

   cout << "What about different types?\n";
   cout << "Between " << f1 << " and " << n2 << " the minimum is : ";
   cout << Pic10C_min<Fraction,int,Fraction>(f1,n2) << "\n";

   cout << "What about int and double types?\n";
   cout << "Between " << n1 << " and " << d2 << " the minimum is : ";
   cout << Pic10C_min<int,double,double>(n1,d2) << "\n";

   return 0;
}
